package com.trellodecoding;

/**
 * Created by cyn on 12/30/2015.
 */
public class TrelloDecodingException extends Exception{
    TrelloDecodingException(String message){
        super(message);
    }
}
