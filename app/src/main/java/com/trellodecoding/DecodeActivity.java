package com.trellodecoding;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.support.design.widget.Snackbar;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.math.BigInteger;

public class DecodeActivity extends AppCompatActivity {

    private static final BigInteger BIG_INTEGER_SEVEN = new BigInteger("7");
    private static final BigInteger BIG_INTEGER_THIRTY_SEVEN = new BigInteger("37");
    private static final String LETTERS = "acdegilmnoprstuw";
    private static final int LENGTH_LETTERS = LETTERS.length();
    private static final String ERROR = "error";
    private EditText editHash;
    private TextView txtResult;
    private Button btnDecode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_decode);

        // Get elements of the view
        editHash = (EditText) findViewById(R.id.editHash);
        btnDecode = (Button) findViewById(R.id.btnDecode);
        txtResult = (TextView) findViewById(R.id.txtResult);

        btnDecode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.setEnabled(false);
                String hash = editHash.getText().toString();
                if(!hash.isEmpty()) {
                    new DecodeHashToString().execute(hash);
                }else{
                    v.setEnabled(true);
                    txtResult.setVisibility(View.INVISIBLE);
                    Snackbar.make(v,R.string.str_empty,Snackbar.LENGTH_SHORT).show();
                }
            }
        });
    }

    private class DecodeHashToString extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            String result;
            try {
                result = decodeHashToString(params[0]);
            } catch (NullPointerException | ArithmeticException | TrelloDecodingException e) {
                result = "error";
            }
            return result;
        }

        @Override
        protected void onPostExecute(String s) {
            btnDecode.setEnabled(true);
            txtResult.setVisibility(View.VISIBLE);
            if(s.equals(ERROR) || s.isEmpty()) {
                txtResult.setText(R.string.str_invalid_hash);
            }else {
                txtResult.setText(String.format(getString(R.string.str_result), s));
            }
        }

        /**
         * Function that decodes a given hash to a string
         * @param hash value to be decoded
         * @return decoded string
         */
        private String decodeHashToString(String hash) throws NullPointerException, ArithmeticException, TrelloDecodingException {
            StringBuilder result = new StringBuilder();
            BigInteger n = new BigInteger(hash);
            BigInteger[] bigIntegersResults;
            int index = 0;
            //Do the cycle until n > 7
            while (n.compareTo(BIG_INTEGER_SEVEN) > 0) {
                //n / 37 at index 0
                //n % 37 at index 1
                bigIntegersResults = n.divideAndRemainder(BIG_INTEGER_THIRTY_SEVEN);
                //Update n value
                n = bigIntegersResults[0];
                //Get the index
                index = bigIntegersResults[1].intValue();
                //If index > LENGTH_LETTERS it means that this letter does not belong to the set
                if(index > LENGTH_LETTERS){
                    throw new TrelloDecodingException("This hash can not be decoded");
                }
                result.append(LETTERS.charAt(index));
            }
            //Reverse the string because we begin to decode backwards: last letter first and so on
            return result.reverse().toString();
        }
    }
}
